package git.doomshade.weapongrowth.files;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.bukkit.configuration.file.YamlConfiguration;

import git.doomshade.weapongrowth.WeaponGrowth;

public class Files extends Settings {
	private File playerData, attributesFile;
	private final String PATH;
	private InputStream attributes;

	public Files(WeaponGrowth plugin) {
		super(plugin);
		this.PATH = plugin.getDataFolder() + File.separator;
		playerData = new File(PATH + "playerdata.dat");
		attributes = plugin.getResource("attributes.yml");
		attributesFile = new File(PATH + "attributes.yml");
	}

	@SuppressWarnings("deprecation")
	public void init() throws IOException {
		WeaponGrowth.getConfigSettings().load();

		if (!playerData.exists()) {
			playerData.createNewFile();
		}

		if (!attributesFile.exists()) {
			try {
				attributesFile.createNewFile();
				YamlConfiguration.loadConfiguration(attributes).save(attributesFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public File getPlayerDataFile() {
		return playerData;
	}

	public File getAttributeFile() {
		return attributesFile;
	}
}
