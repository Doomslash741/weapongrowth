package git.doomshade.weapongrowth.files;

import org.bukkit.ChatColor;

import git.doomshade.weapongrowth.WeaponGrowth;

public class ConfigSettings extends Settings {

	public ConfigSettings(WeaponGrowth plugin) {
		super(plugin);
	}

	public void reload() {
		plugin.reloadConfig();
	}

	public void load() {
		if (!plugin.getDataFolder().isDirectory()) {
			plugin.getDataFolder().mkdirs();
		}
		plugin.saveDefaultConfig();
	}

	public String getOwnerPrefix() {
		return getConfig().isString("owner")
				? ChatColor.translateAlternateColorCodes('&', getConfig().getString("owner"))
				: "&aOwner:";
	}

}
