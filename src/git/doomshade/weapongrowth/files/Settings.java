package git.doomshade.weapongrowth.files;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import git.doomshade.weapongrowth.WeaponGrowth;

public abstract class Settings {
	protected final WeaponGrowth plugin;

	public Settings(WeaponGrowth plugin) {
		this.plugin = plugin;
	}
	
	public ConfigurationSection getAttributes() {
		return getConfig().getConfigurationSection("attributes");
	}
	
	protected FileConfiguration getConfig() {
		return plugin.getConfig();
	}
}
