package git.doomshade.weapongrowth.files;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import git.doomshade.weapongrowth.WeaponGrowth;
import git.doomshade.weapongrowth.events.OnWeaponUpgrade;
import git.doomshade.weapongrowth.weaponstuff.Weapon;

public class User {
	private final Player hrac;
	private File playerData;
	private FileConfiguration fc;
	private ConfigurationSection playerSection;

	public User(Player hrac) {
		this.hrac = hrac;
		this.playerData = WeaponGrowth.getFileSettings().getPlayerDataFile();
		this.fc = YamlConfiguration.loadConfiguration(playerData);
		this.playerSection = fc.getConfigurationSection(hrac.getUniqueId().toString());
	}

	public void addWeapon(Weapon wep) {
		addWeapon(wep.getItem());
	}
	
	public void addWeapon(ItemStack item) {
		List<String> list = playerSection.getStringList(ESettings.WEAPONS.toString());
		if (list.contains(item.getItemMeta().getDisplayName())) {
			return;
		}
		list.add(item.getItemMeta().getDisplayName());
		playerSection.set(ESettings.WEAPONS.toString(), list);
		try {
			fc.save(playerData);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Weapon[] getPlayerWeapons() {
		return null;
	}

	public void initPlayer() {
		if (fc.isConfigurationSection(hrac.getUniqueId().toString())) {
			return;
		}

		ConfigurationSection sec = fc.createSection(hrac.getUniqueId().toString());
		sec.set(ESettings.PLAYER_NAME.toString(), hrac.getName());
		sec.set(ESettings.WEAPONS.toString(), new ArrayList<String>());
		try {
			fc.save(playerData);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void updateWeapons() {
		//int lvl = hrac.getLevel();
		Bukkit.getPluginManager().callEvent(new OnWeaponUpgrade(this));
	}

	public enum ESettings {
		PLAYER_NAME("player-name"), WEAPONS("weapons");

		private String s;

		private ESettings(String s) {
			this.s = s;
		}

		@Override
		public String toString() {
			return s;
		}
	}

}
