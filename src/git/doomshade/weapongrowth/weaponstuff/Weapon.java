package git.doomshade.weapongrowth.weaponstuff;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import git.doomshade.weapongrowth.WeaponGrowth;
import git.doomshade.weapongrowth.exceptions.InvalidItemException;

public class Weapon {
	private final ItemStack item;

	public Weapon(ItemStack item) {
		if (item == null) {
			throw new InvalidItemException("No item in hand");
		}
		if (!item.hasItemMeta() || !(item.getItemMeta().hasLore() || item.getItemMeta().hasDisplayName())) {
			throw new InvalidItemException("Item exception: \n" + "meta: " + item.hasItemMeta()
					+ (item.hasItemMeta() == true
							? "\nlore: " + item.getItemMeta().hasLore() + "\ndisplay name: "
									+ item.getItemMeta().hasDisplayName()
							: ""));
		}
		this.item = item;
	}

	public ItemStack getItem() {
		return item;
	}

	public Set<Attribute> getAttributes() {
		return Attribute.getAttributes(this);
	}

	public Player getOwner() {
		for (String s : item.getItemMeta().getLore()) {
			if (s.startsWith(WeaponGrowth.getConfigSettings().getOwnerPrefix())) {
				Player p = Bukkit.getPlayer(s.replaceAll(WeaponGrowth.getConfigSettings().getOwnerPrefix(), ""));
				if (p.isValid()) {
					return p;
				}
				throw new InvalidItemException("Invalid player name in lore! (" + item.getItemMeta().getDisplayName() + ")");
			}
		}

		throw new InvalidItemException("Item has no owner! (" + item.getItemMeta().getDisplayName() + ")");
	}

}
