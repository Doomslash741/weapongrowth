package git.doomshade.weapongrowth.weaponstuff;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import git.doomshade.weapongrowth.WeaponGrowth;

public class Attribute {
	private String displayName;
	private byte initialCost, costPerLevel;
	private Map<Integer, Byte> additionalCosts;
	private static Set<Attribute> attributes;

	private Attribute(String displayName, byte initialCost, byte costPerLevel, Map<Integer, Byte> additionalCosts) {
		this.displayName = displayName;
		this.initialCost = initialCost;
		this.costPerLevel = costPerLevel;
		this.additionalCosts = additionalCosts;
	}

	public byte getInitialCost() {
		return initialCost;
	}

	public byte getCostPerLevel() {
		return costPerLevel;
	}

	public Map<Integer, Byte> getAdditionalCosts() {
		return additionalCosts;
	}

	public static Set<Attribute> getAttributes(ItemStack item) {
		Set<Attribute> itemAttributes = new HashSet<>();
		return itemAttributes;
	}

	public static Set<Attribute> getAttributes(Weapon item) {
		return getAttributes(item.getItem());
	}
	public static void addAttributes() {
		attributes = new HashSet<>();
		File attrFile = WeaponGrowth.getFileSettings().getAttributeFile();
		FileConfiguration fc = YamlConfiguration.loadConfiguration(attrFile);
		ConfigurationSection cs;
		ConfigurationSection addCosts;
		Map<Integer, Byte> map;
		for (String s : fc.getKeys(false)) {
			cs = fc.getConfigurationSection(s);
			addCosts = cs.getConfigurationSection(EAttribute.ADDITIONAL_COST.toString());
			map = new HashMap<>();
			for (String cskey : addCosts.getKeys(false)) {
				try {
					map.put(Integer.parseInt(cskey), (byte) addCosts.getInt(cskey));
				} catch (NumberFormatException e) {
					continue;
				}
			}
			attributes.add(new Attribute(ChatColor.translateAlternateColorCodes('&', cs.getString(EAttribute.NAME.toString())),
					(byte) cs.getInt(EAttribute.INIT_COST.toString()),
					(byte) cs.getInt(EAttribute.COST_PER_LEVEL.toString()), map));
		}
	}

	public static Set<Attribute> getAttributes() {
		return attributes;
	}

	public String getDisplayName() {
		return displayName;
	}
	
	@Override
	public String toString() {
		return "Name: " + getDisplayName() + ChatColor.stripColor("\nInitial Cost: " + getInitialCost() + "\nCost Per Level: " + getCostPerLevel() + "\nAdditional Costs: " + getAdditionalCosts());
	}

	public enum EAttribute {
		NAME("name"), INIT_COST("initial-cost"), COST_PER_LEVEL("cost-per-level"), ADDITIONAL_COST("additional-cost");

		private String s;

		private EAttribute(String s) {
			this.s = s;
		}

		@Override
		public String toString() {
			return s;
		}
	}
}
