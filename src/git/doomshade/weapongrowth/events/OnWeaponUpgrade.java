package git.doomshade.weapongrowth.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import git.doomshade.weapongrowth.files.User;

public class OnWeaponUpgrade extends Event {
	private static final HandlerList handlers = new HandlerList();
	private User user;

	@Override
	public HandlerList getHandlers() {
		// TODO Auto-generated method stub
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public OnWeaponUpgrade(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

}
