package git.doomshade.weapongrowth.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import git.doomshade.weapongrowth.WeaponGrowth;

public abstract class AbstractCommand implements CommandExecutor {
	protected final WeaponGrowth plugin;

	public AbstractCommand(WeaponGrowth plugin) {
		this.plugin = plugin;
		this.plugin.getCommand(getCommand()).setExecutor(this);
	}
	
	@Override
	public abstract boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);
	
	public abstract String getCommand();
}
