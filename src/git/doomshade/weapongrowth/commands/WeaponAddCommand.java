package git.doomshade.weapongrowth.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import git.doomshade.weapongrowth.WeaponGrowth;
import git.doomshade.weapongrowth.files.User;
import git.doomshade.weapongrowth.weaponstuff.Attribute;
import git.doomshade.weapongrowth.weaponstuff.Weapon;

public class WeaponAddCommand extends AbstractCommand {

	public WeaponAddCommand(WeaponGrowth plugin) {
		super(plugin);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}
		Player hrac = (Player) sender;
		new User(hrac).addWeapon(new Weapon(hrac.getInventory().getItemInMainHand()));
		// TODO Auto-generated method stub
		for (Attribute s : Attribute.getAttributes()) {
			Bukkit.broadcastMessage(s.toString());
		}
		return true;
	}

	@Override
	public String getCommand() {
		// TODO Auto-generated method stub
		return "addtest";
	}

}
