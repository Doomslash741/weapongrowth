package git.doomshade.weapongrowth;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import git.doomshade.weapongrowth.commands.WeaponAddCommand;
import git.doomshade.weapongrowth.files.ConfigSettings;
import git.doomshade.weapongrowth.files.Files;
import git.doomshade.weapongrowth.listeners.PlayerListener;
import git.doomshade.weapongrowth.weaponstuff.Attribute;

public class WeaponGrowth extends JavaPlugin {

	@Override
	public void onEnable() {
		loadFiles();
		initCommands();
		registerListeners();
		
	}
	
	public static void loadFiles() {
		try {
			getFileSettings().init();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Attribute.addAttributes();
	}
	
	public static ConfigSettings getConfigSettings() {
		return new ConfigSettings(JavaPlugin.getPlugin(WeaponGrowth.class));
	}
	public static Files getFileSettings() {
		return new Files(JavaPlugin.getPlugin(WeaponGrowth.class));
	}
	
	private void initCommands() {
		new WeaponAddCommand(this);
	}
	
	private void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new PlayerListener(), this);
	}
}
