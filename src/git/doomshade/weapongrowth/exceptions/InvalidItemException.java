package git.doomshade.weapongrowth.exceptions;

public class InvalidItemException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5611179253919570662L;

	public InvalidItemException(String message) {
		super(message);
	}

}
