package git.doomshade.weapongrowth.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;

import git.doomshade.weapongrowth.files.User;

public class PlayerListener implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		new User(e.getPlayer()).initPlayer();
	}
	
	@EventHandler
	public void onLevelUp(PlayerLevelChangeEvent e) {
		new User(e.getPlayer()).updateWeapons();
	}
}
